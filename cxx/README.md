# Semi-original C++ Code for FreeCT

This directory is kept around for reference, but none of its source code is used in the current versions of FreeCT.  It is, however, a pretty good roadmap for enabling GPU support in a future release.