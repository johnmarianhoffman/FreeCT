package cmd

import (
	"reflect"
	"strings"
	"gitlab.com/freect/freect/pkg/raw"
	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	"fmt"
)

var in = &raw.RawInputConfig{}
var out = &raw.RawOutputConfig{}

func GenerateInputFlags(cmd *cobra.Command, r *raw.RawInputConfig) {
	v := reflect.ValueOf(r).Elem()
	t := reflect.TypeOf(*r)

	for _, curr_field := range reflect.VisibleFields(t) {
		flag_string := "input." + strings.ToLower(curr_field.Name)

		field_val := v.FieldByName(curr_field.Name)

		if field_val.Kind() != reflect.String {
			log.Fatalf("Could not parse field %s as input flag (only strings accepted, not %s)", curr_field.Name, field_val.Kind().String())
		}

		p := field_val.Addr().Interface().(*string)
		cmd.Flags().StringVar(p, flag_string, "", fmt.Sprintf("Use input type %s", curr_field.Name))
	}
}

func GenerateOutputFlags(cmd *cobra.Command, r *raw.RawOutputConfig) {
	v := reflect.ValueOf(r).Elem()
	t := reflect.TypeOf(*r)

	for _, curr_field := range reflect.VisibleFields(t) {
		flag_string := "output." + strings.ToLower(curr_field.Name)

		field_val := v.FieldByName(curr_field.Name)

		if field_val.Kind() != reflect.String {
			log.Fatalf("Could not parse field %s as output flag (only strings accepted, not %s)", curr_field.Name, field_val.Kind().String())
		}

		p := field_val.Addr().Interface().(*string)
		cmd.Flags().StringVar(p, flag_string, "", fmt.Sprintf("Save to output type %s", curr_field.Name))
	}
}
