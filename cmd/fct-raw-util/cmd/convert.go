/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// convertCmd represents the convert command
var convertCmd = &cobra.Command{
	Use:   "convert",
	Short: "Convert raw data between formats",
	Long: `Convert raw data between supported file formats
    - Only one input and one output format can be specified.
    - To convert to multiple output types, you must make multiple calls to the executable.
TODO: include a usage example.`,
	Run: func(cmd *cobra.Command, args []string) {

		// Verify that the user set exactly one input and exactly one output
		r, err := in.GetReader()
		if err != nil {
			log.Fatal(err)
		}

		w, err := out.GetWriter()
		if err != nil {
			log.Fatal(err)
		}

		err = w.Write(r)
		if err != nil {
			log.Fatal(err)
		}

	},
}

func init() {
	rootCmd.AddCommand(convertCmd)

	GenerateInputFlags(convertCmd, in)
	GenerateOutputFlags(convertCmd, out)

}
