package cmd

import (
	"fmt"
	"log"
	"testing"
	"time"
)

func TestBattelleV1Read(t *testing.T) {

	// Setup input
	in.Battelle = "/Users/jhoffman/Data/ldct/ge_case"

	r, err := in.GetReader()
	if err != nil {
		log.Fatal(err)
	}

	start := time.Now()
	_, _, _, err = r.Read(nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%v,%v\n", r.String(), time.Since(start))

}

func TestSFCTPDRead(t *testing.T) {

	// Setup input
	in.SFCTPD = "test.dcm"

	r, err := in.GetReader()
	if err != nil {
		log.Fatal(err)
	}

	start := time.Now()
	_, _, _, err = r.Read(nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%v,%v\n", r.String(), time.Since(start))

}
