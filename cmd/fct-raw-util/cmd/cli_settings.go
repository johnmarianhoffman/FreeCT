package cmd

import (
	"strconv"
	"strings"
)

	

type CLISettings struct {
	ptrFile        string
	useWFBP        bool
	useSAFIRE      bool
	useADMIRE      bool
	sliceThickness string
	dose           string
	kernel         string
	output         string
	fov            string
	anonymize      bool
	imageOutput    string
	patientName    string
	patientID      string

	start float64
	end float64
	increment float64

	centerX float64
	centerY float64
}

// Default ReconCT recon with this tool is 1.0mm Br45 WFBP.  Load all
// other defaults from the scan file.
func DefaultCLISettings() *CLISettings {
	return &CLISettings{
		useWFBP: true,
		output: "./",
		imageOutput: "./RECONCTIMAGES",
		sliceThickness: "1.0",
		anonymize: true,
	}
}

func (c *CLISettings) GetAlgorithms() ([]string, error) {
	// Note: I'm stubbing out the error here to (1) match signatures
	// across these methods, and (2) make it easy to add some
	// sanity checking in the future if needed (but also, probably, yagni)
	
	algs := make([]string, 0)

	if c.useWFBP {
		algs = append(algs, "Standard - WFBP")
	}

	if c.useSAFIRE {
		algs = append(algs, "Standard - SAFIRE")
	}

	if c.useADMIRE {
		algs = append(algs, "Standard - ADMIRE")
	}

	return algs, nil
}

func (c *CLISettings) GetKernel() ([]string, error)  {
	// Note: I'm stubbing out the error here to (1) match signatures
	// across these methods, and (2) make it easy to add some
	// sanity checking in the future if needed (but also, probably, yagni)
	
	return strings.Split(c.kernel, ","), nil
	
}

func (c *CLISettings) GetFOV() ([]float64, error) {
	fov_strings := strings.Split(c.fov, ",")
	fovs := make([]float64, len(fov_strings))

	for i, curr_fov := range fov_strings {
		val, err := strconv.ParseFloat(curr_fov, 64)
		if err != nil {
			return nil, err
		}
		fovs[i] = val
	}

	return fovs, nil
}


func (c *CLISettings) GetSliceThickness() ([]float64, error) {

	sts_strings := strings.Split(c.sliceThickness, ",")

	sts := make([]float64, len(sts_strings))
	
		
	for i, s := range sts_strings {
		st, err := strconv.ParseFloat(s, 64)
		if err != nil {
			return nil, err
		}
		sts[i] = st
	}

	return sts, nil
}

func (c *CLISettings) GetDose() ([]float64, error) {
	// Get individual doses
	dose_strings := strings.Split(c.dose, ",")
	doses := make([]float64, len(dose_strings))
	for i, str := range dose_strings {
		st, err := strconv.ParseFloat(str, 64)
		if err != nil {
			return nil, err
		}
		doses[i] = st
	}

	return doses, nil
}

func (c *CLISettings) GetPatientID() (string, error) {
	return c.patientID, nil
}