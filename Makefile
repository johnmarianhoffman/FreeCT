#GOOS=linux
#GOARCH=amd64

build: build/fct-wfbp build/fct-anonymize build/fct-raw-util

build/fct-raw-util:
	cd cmd/fct-raw-util/ && GOOS=${GOOS} GOARCH=${GOARCH} go build -o ../../build/fct-raw-util

build/fct-anonymize:
	cd cmd/fct-anonymize/ && GOOS=${GOOS} GOARCH=${GOARCH} go build -o ../../build/fct-anonymize

build/fct-wfbp:
	export GOOS=darwin && export GOARCH=arm64 && $(MAKE) -C cmd/fct-wfbp/ build-platform
	export GOOS=darwin && export GOARCH=amd64 && $(MAKE) -C cmd/fct-wfbp/ build-platform
	export GOOS=windows && export GOARCH=amd64 && $(MAKE) -C cmd/fct-wfbp/ build-platform
	export GOOS=linux && export GOARCH=amd64 && $(MAKE) -C cmd/fct-wfbp/ build-platform

#build/libfct.so:
#	@mkdir -p $(@D)
#	cd internal/libfct && $(MAKE) libfct.so && cp libfct.so ../../build/libfct.so

.PHONY: build/fct-wfbp build/libfct.so