# This python could've (should've?) been a shell script...
import sys
import os

# Code paths
experiment_dir = os.getcwd()
code_dir = "/Users/jhoffman/Code/"
go_dicom_dir = os.path.join(code_dir, "go-dicom")
raw_util_dir = os.path.join(code_dir, "freect", "cmd", "fct-raw-util")

# Data paths
battelle_path = "/Users/jhoffman/Data/ldct/ge_case/"
single_file_path = "/Users/jhoffman/Data/ldct/ge-case.dcm"
output_path = os.path.join(experiment_dir, "results.csv")

# Configuration
runs = 3


def set_go_dicom_branch(branch_name):
    os.chdir(go_dicom_dir)
    os.system("git checkout " + branch_name)


# Run un-optimized tests
set_go_dicom_branch("org-main")
os.chdir(raw_util_dir)
os.system("go build")
os.system(
    "./fct-raw-util timedread --input.battelle   {} --repeat {} --note 'unoptimized' | tee -a {}".format(
        battelle_path, runs, output_path
    )
)
os.system(
    "./fct-raw-util timedread --input.sfctpd {} --repeat {} --note 'unoptimized' | tee -a {}".format(
        single_file_path, runs, output_path
    )
)

# Run optimized tests
set_go_dicom_branch("main")
os.chdir(raw_util_dir)
os.system("go build")
os.system(
    "./fct-raw-util timedread --input.battelle   {} --repeat {} --note 'optimized' | tee -a {}".format(
        battelle_path, runs, output_path
    )
)
os.system(
    "./fct-raw-util timedread --input.sfctpd {} --repeat {} --note 'optimized' | tee -a {}".format(
        single_file_path, runs, output_path
    )
)
