module gitlab.com/freect/freect

go 1.18

require (
	github.com/matryer/is v1.4.1
	github.com/mjibson/go-dsp v0.0.0-20180508042940-11479a337f12
	github.com/sirupsen/logrus v1.9.3
	github.com/spf13/cobra v1.5.0
	github.com/suyashkumar/dicom v1.0.7
	gitlab.com/freect/go-dicom v1.1.2
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/text v0.3.8 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
