package rebin

import (
	"errors"

	log "github.com/sirupsen/logrus"

	"math"
	"sync"

	"gitlab.com/freect/freect/pkg/raw"
	"gitlab.com/freect/freect/pkg/util"
)

var _ RebinFunc = RebinZFFS // Compile time check that we satisfy rebin function signature

// During rebinning, channels are always oversampled to improved
// spatial resolution in the final image.

// All math on the CPU should be done with Float64 precision and only
// cast back to Float32 for the final storage back into our array.
// This is won't be true for GPU.

// TODO: I can't help but worry about the cache hit rate with this
// implementation.  Looks like it'll be super ugly.

// Although this implementation avoids an extra copy of the data, I
// think I'm likely to cause some confusion in here since I'm updating
// the projection data pointers quietly behind the scenes, and then
// still returning the original raw projection array.  I think this is
// likely to cause issues at some point in the future.
func RebinZFFS(scan_geom *raw.ScanGeometry, projs []*raw.Projection) ([]*raw.Projection, error) {

	// Z rebin takes place in three stages
	// (1) Split data into stacks by focal spot
	// (2) Rebin each focal spot independently
	// (3) Interleave the rows of the two rebinned projection stacks
	//
	// The final projection stack will have *half* the number of
	// projections, but each projection will have double the number of
	// rows.  Additionally, we oversample channels by a factor of 2,
	// as we do in rebin_noffs.

	if scan_geom.AnodeAngle == 0.0 {
		return nil, errors.New("rebin not possible: scan_geom.AnodeAngle must be non-zero. If you don't know, ")
	}

	reverse_row_interleave := projs[0].RowsFlipped

	n_rows := scan_geom.DetectorRows
	n_chan := scan_geom.DetectorChannels
	n_proj := len(projs)

	n_rows_final := 2 * n_rows
	n_chan_final := 2 * n_chan
	n_proj_final := n_proj / 2

	output_central_channel := 2.0 * projs[0].CentralChannel
	output_channel_width := 0.5 * projs[0].ChannelWidth
	output_row_width := 0.5 * projs[0].RowWidth

	log.Warn("central channel: ", projs[0].CentralChannel)
	log.Warn("central channel output: ", output_central_channel)

	// Split the projections by focal spot
	ffs_1 := make([]*raw.Projection, n_proj_final)
	ffs_2 := make([]*raw.Projection, n_proj_final)

	for i, v := range projs {
		if i%2 == 0 {
			ffs_1[i/2] = v
		} else {
			ffs_2[i/2] = v
		}
	}

	// Rebin each stack
	//r_f := scan_geom.DistSourceToIsocenter
	//d_beta := math.Asin(scan_geom.DetectorTransverseSpacing / scan_geom.DistSourceToDetector)
	//d_B := scan_geom.DistSourceToIsocenter * math.Sin(d_beta/2.0)          // Detector grid resampled as if at isocenter
	d_theta := 2 * math.Pi / float64(scan_geom.ProjectionsPerRotation/2.0) // TODO: Double check the value that comes out of here

	dr := FFSSupport_DelR(scan_geom)

	//log.Info("dr is: ", dr)
	//da := 0.0

	lookup_1 := FFSSupport_CalculateBetaLookup(scan_geom, -dr, 0.0)
	lookup_2 := FFSSupport_CalculateBetaLookup(scan_geom, dr, 0.0)

	// Rebin focal spots separately
	rebinned_projs := make([]float32, n_rows_final*n_chan_final*n_proj_final)
	fan_angle_increment := math.Asin(scan_geom.DetectorTransverseSpacing / scan_geom.DistSourceToDetector)
	dB := scan_geom.DistSourceToIsocenter * math.Sin(fan_angle_increment/2.0)
	d_alpha := 2.0 * math.Pi / float64(scan_geom.ProjectionsPerRotation)

	rebinRows := func(row_idxs []int, wg *sync.WaitGroup) {
		for _, curr_row := range row_idxs {
			util.LogUpdate(curr_row+1, n_rows_final, 10, "rebinning") // TODO: double check the n_rows value in this line

			rho := (curr_row) % 2
			dr_row := dr * math.Pow(-1.0, float64(rho+1)) // rho==0 -> -dr ; rho==1 -> dr; rho==2 -> -dr; ...

			r_fr := FFSSupport_RFr(scan_geom.DistSourceToIsocenter, dr_row, 0.0)
			//r_fr := scan_geom.DistSourceToIsocenter + dr_row // Don't have to do the full calculation since da disappears

			for curr_chan := 0; curr_chan < n_chan_final; curr_chan++ {
				b := (float64(curr_chan) - output_central_channel) * dB

				// Compute the source locations for the current output
				beta_rk := math.Asin(b / r_fr)

				beta_idx := 0.0
				if rho == 0 {
					beta_idx = FFSSupport_GetBetaIdx(lookup_1, beta_rk)
				} else {
					beta_idx = FFSSupport_GetBetaIdx(lookup_2, beta_rk)
				}

				for curr_proj := 0; curr_proj < n_proj_final; curr_proj++ {

					// First, compute our output coordinates based on the current pixel
					theta := float64(curr_proj) * d_theta
					alpha := theta - beta_rk

					// Convert source locations into indices into our
					// source array NOTE: this could be one spot to
					// handle the exact tube angle/table
					// position. Here, I'm assuming perfect spacing
					// delta_alpha.
					alpha_idx := alpha / d_alpha
					row_idx := curr_row / 2 // index into the separated stacks (which have 1/2 the number of rows as the final output array)
					val := 0.0

					if rho == 0 {
						alpha_idx = alpha_idx / 2.0
						val = RebinInterp(ffs_1, alpha_idx, beta_idx, row_idx)
					} else {
						alpha_idx = (alpha_idx - 1.0) / 2.0
						val = RebinInterp(ffs_2, alpha_idx, beta_idx, row_idx)
					}

					// Insert into the final output array (interleaved)
					// If the input rows have been reversed, then we need to reverse the interleave order
					// to get the FFS ordering correct
					output_row := curr_row
					if reverse_row_interleave {
						output_row = n_rows_final - 1 - curr_row
					}

					//output_idx := curr_chan + curr_row*n_chan_final + curr_proj*n_chan_final*n_rows_final
					output_idx := curr_chan + output_row*n_chan_final + curr_proj*n_chan_final*n_rows_final
					rebinned_projs[output_idx] = float32(val)
				}
			}
		}

		wg.Done()
	}
	util.LaunchParallelN(rebinRows, n_rows_final, util.MAX_PROC)

	// Map the output data back into our projections.
	// Not sure if this'll actually work. We have 1/2 the number
	// of original projections, tube angles are different, etc.
	// Might just be easier to
	final_projs := make([]*raw.Projection, n_proj_final)
	for i := 0; i < n_proj_final; i++ {
		start_slice := i * n_rows_final * n_chan_final
		end_slice := start_slice + n_rows_final*n_chan_final

		final_projs[i] = &raw.Projection{
			Index:          i,
			NumRows:        n_rows_final,
			NumChannels:    n_chan_final,
			CentralRow:     2.0 * projs[0].CentralRow,
			CentralChannel: output_central_channel,
			ChannelWidth:   output_channel_width,
			RowWidth:       output_row_width,         // TODO: DOUBLE CHECK THIS ONE
			TablePosition:  projs[2*i].TablePosition, // TODO: DOUBLE CHECK THIS ONE
			TubeAngle:      projs[2*i].TubeAngle,     // TODO: DOUBLE CHECK THIS ONE
			Rebinned:       true,
			Filtered:       false,
			Data:           rebinned_projs[start_slice:end_slice],
		}

	}

	// Due to how I handle the reversed row interleaving above, we
	// need to execute this last flip (if reversed above) This is not
	// the most efficient way to do this, however at only ~50ms, it's
	// still quite small relative to the other reconstruction steps.
	if reverse_row_interleave {
		raw.ProjectionStack(final_projs).ReverseRowOrder()
	}

	return final_projs, nil
}
