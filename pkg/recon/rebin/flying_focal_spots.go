// This file contains helper functions derived from Flohr et. al. that
// support rebinning with flying focal spots.
package rebin

import (
	"math"

	"gitlab.com/freect/freect/pkg/raw"
)

func FFSSupport_CalculateBetaLookup(sg *raw.ScanGeometry, dr, da float64) []float64 {

	//n_output_channels := 2 * sg.DetectorChannels // Ultimately, this should be a tuneable upscaling parameter

	lookup := make([]float64, sg.DetectorChannels)

	for i := range lookup {
		lookup[i] = FFSSupport_BetaRK(sg, dr, da, i)
	}

	return lookup
}

func FFSSupport_GetBetaIdx(lookup []float64, beta float64) float64 {

	idx_low := 0
	for ; idx_low < len(lookup)-1; idx_low++ {
		if beta < lookup[idx_low] {
			break
		}
	}

	if idx_low == 0 {
		idx_low = 1
	}

	return float64(idx_low) - 1.0 + (beta-lookup[idx_low-1])/(lookup[idx_low]-lookup[idx_low-1])
}

// Equation (2a)
// da here is da, not dalpha
func FFSSupport_FRho(rf, dr, da float64) (float64, float64) {
	return rf + dr, da
}

// Equation (2b)
// k is the channel index
func FFSSupport_Dk(sg *raw.ScanGeometry, k int) (float64, float64) {
	fan_angle_increment := math.Asin(sg.DetectorTransverseSpacing / sg.DistSourceToDetector)
	r_f := sg.DistSourceToIsocenter
	r_fd := sg.DistSourceToDetector
	b_0_k := (float64(k) - sg.DetectorCentralChannel) * fan_angle_increment

	return r_f - r_fd*math.Cos(b_0_k), -r_fd * math.Sin(b_0_k)
}

// Equation (3a) from Flohr et al.
func FFSSupport_DelR(sg *raw.ScanGeometry) float64 {
	nom_slice_thickness_at_iso := (sg.DistSourceToIsocenter / sg.DistSourceToDetector) * sg.DetectorAxialSpacing

	r_fd := sg.DistSourceToDetector
	r_f := sg.DistSourceToIsocenter
	return (r_fd * nom_slice_thickness_at_iso) / (4 * (r_fd - r_f) * math.Tan(sg.AnodeAngle))
}

// Equation (3b) from Flohr et al.
func FFSSupport_DelA(sg *raw.ScanGeometry) float64 {
	fan_angle_increment := math.Asin(sg.DetectorTransverseSpacing / sg.DistSourceToDetector)
	r_fd := sg.DistSourceToDetector
	r_f := sg.DistSourceToIsocenter
	return r_f * r_fd * math.Sin(fan_angle_increment) / (4 * (r_fd - r_f))
}

// Equation (8)
// k is channel index
func FFSSupport_BetaRK(sg *raw.ScanGeometry, dr, da float64, k int) float64 {
	x1, x2 := FFSSupport_FRho(sg.DistSourceToIsocenter, dr, da)
	y1, y2 := FFSSupport_Dk(sg, k)

	// angle(-f, dk-f)
	return FFSSupport_Angle(-x1, -x2, y1-x1, y2-x2)
}

// Equation (9) from Flohr et al.
func FFSSupport_Angle(x1, x2, y1, y2 float64) float64 {
	return math.Asin((x1*y2 - x2*y1) / (math.Sqrt(x1*x1+x2*x2) * math.Sqrt(y1*y1+y2*y2)))
}

// Equation (12)
// NOTE: this is dalpha, NOT da
func FFSSupport_DAlphaR(rf float64, dr float64, da float64) float64 {
	f1, f2 := FFSSupport_FRho(rf, dr, da)
	return FFSSupport_Angle(rf, 0.0, f1, f2)
}

// Equation (13)
// NOTE: da here is da, not dalpha
// rf is the focal radius of the scanner (sg.DistanceSourceToIsocenter)
func FFSSupport_RFr(rf float64, dr float64, da float64) float64 {
	return math.Sqrt((rf+dr)*(rf+dr) + da*da)
}
