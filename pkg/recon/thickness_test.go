package recon

import (
	"testing"

	"gitlab.com/freect/freect/pkg/image"
)

func TestThickness(t *testing.T) {
	output_volume := &ReconVolume{
		StartPosition:  -10,
		EndPosition:    10.0,
		SliceThickness: 2.0,
		SliceSpacing:   2.0,
		Nx:             4,
		Ny:             4,
	}

	native_slice_thickness := 0.6
	slice_locations := image.GenerateSliceLocations(output_volume.StartPosition, output_volume.EndPosition, native_slice_thickness)

	input_stack := make([]*image.ReconstructedImage, len(slice_locations))
	for i := range input_stack {
		input_stack[i] = &image.ReconstructedImage{
			Nx:             output_volume.Nx,
			Ny:             output_volume.Ny,
			SliceThickness: native_slice_thickness,
			Z:              slice_locations[i],
		}

	}

}
