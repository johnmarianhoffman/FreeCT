package recon

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/freect/freect/pkg/raw"
)

func Preprocess(preprocessing_config *PreprocessingConfig, projs []*raw.Projection) ([]*raw.Projection, error) {

	if preprocessing_config.CorrectMATLABIndexing {
		log.Info("preprocessing: correcting matlab indexing")
		raw.ProjectionStack(projs).CorrectMATLABIndexing()
	}

	if preprocessing_config.FlipDetectorChannels {
		log.Info("preprocessing: flipping detector channels")
		raw.ProjectionStack(projs).ReverseChannelOrder()
	}

	if preprocessing_config.FlipDetectorRows {
		log.Info("preprocessing: flipping detector rows")
		raw.ProjectionStack(projs).ReverseRowOrder()
	}

	if preprocessing_config.NegateTubeAngle {
		log.Info("preprocessing: negating tube angle")
		raw.ProjectionStack(projs).NegateTubeAngle()
	}

	if preprocessing_config.AdapativeFiltration > 0 {
		log.Info("preprocessing: adaptive filtration kachelreiss")
		filter_func := AdaptiveFiltrationKachelreiss
		raw.ProjectionStack(projs).AdaptiveFiltration(filter_func)
	}

	return projs, nil
}
