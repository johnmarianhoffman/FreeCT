package raw

type ProjectionReader interface {
	ReadProjection() (*Projection, error)
}
