package raw

// ScanMetadata describes information about a scan unrelated to geometry and
// should not be modified once read from the raw data.
// ScanMetadata should be treated as a read-only struct throughout FreeCT
// applications.
type ScanMetadata struct {
	// Header
	ImplementationClassUID string `json:"ImplementationClassUID" yaml:"ImplementationClassUID"`
	ImplementationVersion  string `json:"ImplementationVersion" yaml:"ImplementationVersion"`
	SourceApplication      string `json:"SourceApplicationEntityName" yaml:"SourceApplicationEntityName"`

	// Dataset
	SOPClassUID     string `json:"SOPClassUID" yaml:"SOPClassUID"`
	SOPInstanceUID  string `json:"SOPInstanceUID" yaml:"SOPInstanceUID"`
	StudyDate       string `json:"StudyDate" yaml:"StudyDate"`
	SeriesDate      string `json:"SeriesDate" yaml:"SeriesDate"`
	ContentDate     string `json:"ContentDate" yaml:"ContentDate"`
	StudyTime       string `json:"StudyTime" yaml:"StudyTime"`
	ContentTime     string `json:"ContentTime" yaml:"ContentTime"`
	AccessionNumber string `json:"AccessionNumber" yaml:"AccessionNumber"`

	ReferringPhysicianName string `json:"ReferringPhysicianName" yaml:"ReferringPhysicianName"`
	SeriesDescription      string `json:"SeriesDescription" yaml:"SeriesDescription"`
	IrradiationEventUID    string `json:"IrradiationEventUID" yaml:"IrradiationEventUID"`
	CreatorVersionUID      string `json:"CreatorVersionUID" yaml:"CreatorVersionUID"`

	PatientName            string `json:"PatientName" yaml:"PatientName"`
	PatientID              string `json:"PatientID" yaml:"PatientID"`
	PatientBirthDate       string `json:"PatientBirthDate" yaml:"PatientBirthDate"`
	PatientSex             string `json:"PatientSex" yaml:"PatientSex"`
	PatientAge             string `json:"PatientAge" yaml:"PatientAge"`
	PatientIdentityRemoved string `json:"PatientIdentityRemoved" yaml:"PatientIdentityRemoved"`
	DeidentificationMethod string `json:"DeidentificationMethod" yaml:"DeidentificationMethod"`

	RescaleIntercept  float64 `json:"RescaleIntercept" yaml:"RescaleIntercept"`
	RescaleSlope      float64 `json:"RescaleSlope" yaml:"RescaleSlope"`
	SpiralPitchFactor float64 `json:"SpiralPitchFactor" yaml:"SpiralPitchFactor"`
	BodyPartExamined  string  `json:"BodyPartExamined" yaml:"BodyPartExamined"`
	ProtocolName      string  `json:"ProtocolName" yaml:"ProtocolName"`
	KVP               int     `json:"KVP" yaml:"KVP"`
	ExposureTime      int     `json:"ExposureTime" yaml:"ExposureTime"`
	XRayTubeCurrent   int     `json:"XRayTubeCurrent" yaml:"XRayTubeCurrent"`
	PatientPosition   string  `json:"PatientPosition" yaml:"PatientPosition"`

	StudyInstanceUID  string  `json:"StudyInstanceUID" yaml:"StudyInstanceUID"`
	SeriesInstanceUID string  `json:"SeriesInstanceUID" yaml:"SeriesInstanceUID"`
	StudyID           string  `json:"StudyID" yaml:"StudyID"`
	SeriesNumber      int     `json:"SeriesNumber" yaml:"SeriesNumber"`
	InstanceNumber    int     `json:"InstanceNumber" yaml:"InstanceNumber"`
	TimeStamp         float64 `json:"TimeStamp" yaml:"TimeStamp"` // 7033, 1067

	FrameOfReferenceUID       string `json:"FrameOfReferenceUID" yaml:"FrameOfReferenceUID"`
	PhotometricInterpretation string `json:"PhotometricInterpretation" yaml:"PhotometricInterpretation"`
	HighBit                   int    `json:"HighBit" yaml:"HighBit"`
	PixelRepresentation       int    `json:"PixelRepresentation" yaml:"PixelRepresentation"`

	SourceAngularPositionShift float64 `json:"SourceAngularPositionShift" yaml:"SourceAngularPositionShift"` // 7033, 100b
	SourceAxialPositionShift   float64 `json:"SourceAxialPositionShift" yaml:"SourceAxialPositionShift"`     // 7033, 100c
	SourceRadialDistanceShift  float64 `json:"SourceRadialDistanceShift" yaml:"SourceRadialDistanceShift"`   // 7033, 100d

	NumberOfSpectra  int     `json:"NumberOfSpectra" yaml:"NumberOfSpectra"`   // 7033, 1061
	SpectrumIndex    int     `json:"SpectrumIndex" yaml:"SpectrumIndex"`       // 7033, 1063
	PhotonStatistics float64 `json:"PhotonStatistics" yaml:"PhotonStatistics"` // 7033, 1065

	BeamHardeningCorrectionFlag string `json:"BeamHardeningCorrectionFlag" yaml:"BeamHardeningCorrectionFlag"` // 7039, 1003
	GainCorrectionFlag          string `json:"GainCorrectionFlag" yaml:"GainCorrectionFlag"`                   // 7039, 1004
	DarkFieldCorrectionFlag     string `json:"DarkFieldCorrectionFlag" yaml:"DarkFieldCorrectionFlag"`         // 7039, 1005
	FlatFieldCorrectionFlag     string `json:"FlatFieldCorrectionFlag" yaml:"FlatFieldCorrectionFlag"`         // 7039, 1006
	BadPixelCorrectionFlag      string `json:"BadPixelCorrectionFlag" yaml:"BadPixelCorrectionFlag"`           // 7039, 1007
	ScatterCorrectionFlag       string `json:"ScatterCorrectionFlag" yaml:"ScatterCorrectionFlag"`             // 7039, 1008
	LogFlag                     string `json:"LogFlag" yaml:"LogFlag"`                                         // 7039, 1009
}
