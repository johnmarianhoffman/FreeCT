package raw

import (
	"fmt"
	"github.com/matryer/is"
	"gopkg.in/yaml.v3"
	
	"strconv"
	"testing"
)

const (
	TEST_BINARY_PATH      = "test_data/binary"
	TEST_BINARY_READ_UTIL = "/Users/jhoffman/Code/FreeCT/cmd/fct-read-util/tmp/"
)

func TestBinaryRead(t *testing.T) {
	is := is.New(t)

	//d := NewLocalDirectoryStore(TEST_BINARY_READ_UTIL)
	d := NewLocalDirectoryStore(TEST_BINARY_PATH)
	b, err := NewBinaryReader(d)
	if err != nil {
		t.Fatal(err)
	}

	g, err := b.ReadGeometry()
	is.NoErr(err)

	p, _, _, err := b.Read(g)
	
	if err != nil {
		t.Error(err)
	}

	if len(p) != 3 {
		t.Errorf("Expected 3 projections but found %d", len(p))
	}

	test_proj := p[0]
	if test_proj.NumRows != 64 {
		t.Error("expected 64 rows but got ", test_proj.NumRows)
	}
	t.Logf("projection has %d rows", test_proj.NumRows)

	if test_proj.NumChannels != 888 {
		t.Error("expected 888 channels but got ", test_proj.NumRows)
	}
	t.Logf("projection has %d channels", test_proj.NumChannels)

	t.Log("Sample attenuation values read in: ")
	for i, v := range test_proj.Data {
		idx := strconv.Itoa(i)
		val := strconv.FormatFloat(float64(v), 'f', 5, 64)

		t.Logf(fmt.Sprintf("%05s %10s", idx, val))

		if i == 10 {
			break
		}
	}

	data, _ := yaml.Marshal(g)
	if g.Manufacturer != "GE" {
		t.Error("expected manufacturer to be GE but got: ", g.Manufacturer)
	}

	t.Log("Geometry read in:\n", string(data))
}
