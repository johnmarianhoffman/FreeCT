package raw

import (
	"fmt"

	"strconv"
	"testing"

	"github.com/matryer/is"
	"gopkg.in/yaml.v3"

	"os"
	"reflect"

	"gitlab.com/freect/go-dicom/pkg/tag"
)

// TODO: Write a test for ReadMetadata

const (
	TEST_BATTELLE_READ_PATH  = "test_data/battelle/"
	TEST_BATTELLE_WRITE_PATH = "./test_data/battelle/write/"
	TEST_SFCTPD_READ_PATH    = "./test_data/SFCTPD/read/"
	TEST_SFCTPD_WRITE_PATH   = "./test_data/SFCTPD/write/v2/"
	REF_BATTELLE_WRITE_PATH  = "./test_data/SFCTPD/write/v1/"

	// Will delete later
	TEST_NEW_SFCTPD_READPATH  = "/radraid/jgenender/LDCTPD/L186/proj/fd"
	TEST_NEW_SFCTPD_WRITEPATH = "/radraid/jgenender/LDCTPD/L186/"
)

func TestNewSFCTPD(t *testing.T) {
	is := is.New(t)

	ls := NewLocalDirectoryStore(TEST_NEW_SFCTPD_READPATH)

	r, err := NewBattelleReader(ls)
	is.NoErr(err)

	wr := NewSFCTPDWriter(TEST_NEW_SFCTPD_WRITEPATH)
	err = wr.Write(r)
	is.NoErr(err)
}

func TestBattelleProjection(t *testing.T) {
	is := is.New(t)

	ls := NewLocalDirectoryStore(TEST_BATTELLE_READ_PATH)

	r, err := NewBattelleReader(ls)
	if err != nil {
		t.Fatal(err)
	}

	g, err := r.ReadGeometry()
	is.NoErr(err) // read geometry from raw file

	p, _, _, err := r.Read(g)

	if err != nil {
		t.Error("dicom read failed: ", err)
	}

	t.Logf("read %d projections", len(p))

	test_proj := p[0]
	if test_proj.NumRows != 64 {
		t.Error("expected 64 rows but got ", test_proj.NumRows)
	}
	t.Logf("projection has %d rows", test_proj.NumRows)

	if test_proj.NumChannels != 888 {
		t.Error("expected 888 channels but got ", test_proj.NumRows)
	}
	t.Logf("projection has %d channels", test_proj.NumChannels)

	if test_proj.TubeAngle != 3.8016846 {
		t.Errorf("expected %0.9f tube angle but got %0.9f", 3.8016846, test_proj.TubeAngle)
	}
	t.Logf("projection has %f tube angle", test_proj.TubeAngle)

	if test_proj.TablePosition != 25.279000 {
		t.Errorf("expected %f table position but got %v", 25.279, test_proj.TubeAngle)
	}
	t.Logf("projection has %f table position", test_proj.TablePosition)

	for i, v := range test_proj.Data {

		idx := strconv.Itoa(i)
		val := strconv.FormatFloat(float64(v), 'f', 5, 64)

		fmt.Printf("%05s %10s\n", idx, val)

		if i == 10 {
			break
		}

	}
}

func TestBattelleGeometry(t *testing.T) {

	ls := NewLocalDirectoryStore(TEST_BATTELLE_READ_PATH)

	r, err := NewBattelleReader(ls)
	if err != nil {
		t.Fatal(err)
	}

	g, err := r.ReadGeometry()
	if err != nil {
		t.Error(err)
	}

	data, err := yaml.Marshal(g)
	if err != nil {
		t.Error(err)
	}

	t.Log(string(data))
}

func TestBattelleMetadata(t *testing.T) {

	ls := NewLocalDirectoryStore(TEST_BATTELLE_READ_PATH)

	r, err := NewBattelleReader(ls)
	if err != nil {
		t.Fatal(err)
	}

	m, err := r.ReadMetadata()
	if err != nil {
		t.Error(err)
	}

	data, err := yaml.Marshal(m)
	if err != nil {
		t.Error(err)
	}

	t.Log(string(data))
}

func TestBattelleWriter(t *testing.T) {
	ls := NewLocalDirectoryStore(REF_BATTELLE_WRITE_PATH)
	wr := NewBattelleWriter(ls, TEST_BATTELLE_WRITE_PATH)

	r, err := NewBattelleReader(ls)
	if err != nil {
		t.Error(err)
	}

	_, err = os.Stat(wr.path)
	if !os.IsNotExist(err) {
		err = os.RemoveAll(wr.path)
		if err != nil {
			t.Error(err)
		}
	}

	err = wr.Write(r)
	if err != nil {
		t.Error(err)
	}

	defer func() {
		if err := os.RemoveAll(TEST_BATTELLE_WRITE_PATH); err != nil {
			t.Error(err)
		}
	}()

	dcm_data_ref, dcm_data_pred, err := openTestDicomDatasets(REF_BATTELLE_WRITE_PATH, wr.path)
	if err != nil {
		t.Error(err)
	}

	// // Test important tags
	// Test Number of Detector Rows
	ref_val_int, pred_val_int, err := getIntTestElement(dcm_data_ref, dcm_data_pred, tag.NumberofDetectorRows)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(ref_val_int, pred_val_int) {
		t.Errorf("Failed because expected %v, got %v", ref_val_int, pred_val_int)
	}

	// Test Number of Detector Channels
	ref_val_int, pred_val_int, err = getIntTestElement(dcm_data_ref, dcm_data_pred, tag.NumberofDetectorColumns)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(ref_val_int, pred_val_int) {
		t.Errorf("Failed because expected %v, got %v", ref_val_int, pred_val_int)
	}

	// Test Tube Angle
	ref_val_float, pred_val_float, err := getFloatTestElement(dcm_data_ref, dcm_data_pred, tag.DetectorFocalCenterAngularPosition)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(ref_val_float, pred_val_float) {
		t.Errorf("Failed because expected %v, got %v", ref_val_float, pred_val_float)
	}

	// Test Table Position
	ref_val_float, pred_val_float, err = getFloatTestElement(dcm_data_ref, dcm_data_pred, tag.DetectorFocalCenterAxialPosition)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(ref_val_float, pred_val_float) {
		t.Errorf("Failed because expected %v, got %v", ref_val_float, pred_val_float)
	}

	// Test Data
	ref_val_pixel, err := readPixelData(&dcm_data_ref, 64, 888)
	if err != nil {
		t.Error(err)
	}

	pred_val_pixel, err := readPixelData(&dcm_data_pred, 64, 888)
	if err != nil {
		t.Error(err)
	}

	for i, val := range ref_val_pixel {
		if (val - pred_val_pixel[i]) > 0.001 { // We expect at least 3 decimal precision
			t.Error("Failed because expected and predicted data do not match: ", err)
		}
	}
}

//func BenchmarkBattelleRead(b *testing.B) {
//	test_dir := "/Users/jhoffman/Data/ldct/sample_case"
//	for n:=0; n< b.N; n++ {
//		r := NewRawDataReader(FromDicom(test_dir))
//		r.Read()
//	}
//}
