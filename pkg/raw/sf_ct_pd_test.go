package raw

import (
	"errors"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	dicom "gitlab.com/freect/go-dicom"
	"gitlab.com/freect/go-dicom/pkg/tag"
	"gopkg.in/yaml.v3"
)

func TestSFCTPDProjection(t *testing.T) {
	// Load reference BattelleV1 projection data
	files, err := os.ReadDir(TEST_BATTELLE_READ_PATH)
	if err != nil {
		t.Error(err)
	}

	// Read test SFCTPD data
	r, err := NewSFCTPDReader(TEST_SFCTPD_READ_PATH)
	if err != nil {
		t.Fatal(err)
	}

	p, _, _, err := r.Read(nil)
	if err != nil {
		t.Error("Failed because Read method could not run: ", err)
	}

	num_proj, err := readNumberOfProjections(&r.data)
	if err != nil {
		t.Error("Failed because could not get number of projections: ", err)
	}

	t.Logf("read %d projections", num_proj)

	for proj_idx := 0; proj_idx < len(files); proj_idx++ {

		filepath := filepath.Join(TEST_BATTELLE_READ_PATH, files[proj_idx].Name())
		f, err := os.Open(filepath)
		if err != nil {
			t.Error(err)
		}

		info, err := os.Stat(filepath)
		if err != nil {
			t.Error(err)
		}

		dcm_data_v1, err := dicom.Parse(f, info.Size(), nil)
		if err != nil {
			t.Error(err)
		}

		if err = f.Close(); err != nil {
			t.Error(err)
		}

		// Test relevant tags on the i-th projection
		test_proj_v2 := p[proj_idx]

		e, err := dcm_data_v1.FindElementByTag(tag.NumberofDetectorRows)
		if err != nil {
			t.Error(err)
		}
		if test_proj_v2.NumRows != int(e.Value.GetValue().([]int)[0]) {
			t.Error("Failed because expected 64 rows but got ", test_proj_v2.NumRows)
		}
		t.Logf("projection %d has %d rows", proj_idx, test_proj_v2.NumRows)

		e, err = dcm_data_v1.FindElementByTag(tag.NumberofDetectorColumns)
		if err != nil {
			t.Error(err)
		}
		if test_proj_v2.NumChannels != int(e.Value.GetValue().([]int)[0]) {
			t.Error("Failed because expected 888 channels but got ", test_proj_v2.NumChannels)
		}
		t.Logf("projection %d has %d channels", proj_idx, test_proj_v2.NumChannels)

		e, err = dcm_data_v1.FindElementByTag(tag.DetectorFocalCenterAngularPosition)
		if err != nil {
			t.Error(err)
		}
		if test_proj_v2.TubeAngle != float64(e.Value.GetValue().([]float64)[0]) {
			t.Errorf("Failed because expected %0.9f tube angle but got %0.9f", float64(e.Value.GetValue().([]float64)[0]), test_proj_v2.TubeAngle)
		}
		t.Logf("projection %d has %f tube angle", proj_idx, test_proj_v2.TubeAngle)

		e, err = dcm_data_v1.FindElementByTag(tag.DetectorFocalCenterAxialPosition)
		if err != nil {
			t.Error(err)
		}
		if test_proj_v2.TablePosition != float64(e.Value.GetValue().([]float64)[0]) {
			t.Errorf("Failed because expected %0.9f table position but got %0.9f", float64(e.Value.GetValue().([]float64)[0]), test_proj_v2.TablePosition)
		}
		t.Logf("projection %d has %f table position", proj_idx, test_proj_v2.TablePosition)

		ref_pixel_v1, err := readPixelData(&dcm_data_v1, test_proj_v2.NumRows, test_proj_v2.NumChannels)
		if err != nil {
			t.Error(err)
		}
		if !reflect.DeepEqual(ref_pixel_v1, test_proj_v2.Data) {
			t.Error("Failed because data was read incorrectly.")
		}
	}
}

func TestSFCTPDGeometry(t *testing.T) {
	files, err := os.ReadDir(TEST_BATTELLE_READ_PATH)
	if err != nil {
		t.Error(err)
	}

	r, err := NewSFCTPDReader(TEST_SFCTPD_READ_PATH)
	if err != nil {
		t.Fatal("Failed because could not create SFCTPD reader: ", err)
	}

	// TODO: Fix this test
	geometry_v1, err := r.ReadGeometry()
	if err != nil {
		t.Error("Failed because ReadGeometry method could not run: ", err)
	}

	geometry_v2 := &ScanGeometry{
		Manufacturer:              "GE",
		DetectorRows:              64,
		DetectorChannels:          888,
		DetectorTransverseSpacing: 1.0239,
		DetectorAxialSpacing:      1.0987823,
		DetectorShape:             "CYLINDRICAL",

		DistSourceToDetector:   946.746,
		DistSourceToIsocenter:  538.52,
		DetectorCentralRow:     32.5,
		DetectorCentralChannel: 444.75,
		ProjectionGeometry:     "FANBEAM",

		ScanType:               "HELICAL",
		ProjectionsPerRotation: 984,
		FlyingFocalSpotMode:    "FFSNONE",

		StartTablePosition:     25.279000,
		EndTablePosition:       25.198809,
		TableFeedPerRotation:   0.984375 * 64 * 538.52 * 1.0987823 / 946.746,
		AcquisitionFieldOfView: 500,
		CollimatedSliceWidth:   538.52 * 1.0987823 / 946.746,

		HUCalibrationFactor: 0.01921,

		NumProjectionsTotal: len(files),
	}

	if !reflect.DeepEqual(geometry_v1, geometry_v2) {
		t.Errorf("Failed because ScanGeometry was read incorrectly: \n%v \n \n%v", geometry_v1, geometry_v2)
	}
}

func TestSFCTPDMetadata(t *testing.T) {

	r, err := NewSFCTPDReader(TEST_SFCTPD_READ_PATH)
	if err != nil {
		t.Fatal("Failed because could not create SFCTPD reader: ", err)
	}

	m, err := r.ReadMetadata()
	if err != nil {
		t.Error("Failed because ReadMetadata method could not run: ", err)
	}

	data, err := yaml.Marshal(m)
	if err != nil {
		t.Error(err)
	}

	t.Log(string(data))
}

func TestSFCTPDWriter(t *testing.T) {
	ls := NewLocalDirectoryStore(REF_BATTELLE_WRITE_PATH)
	wr := NewSFCTPDWriter(TEST_SFCTPD_WRITE_PATH)

	r, err := NewBattelleReader(ls)
	if err != nil {
		t.Error(err)
	}
	_, err = os.Stat(wr.filepath)
	if !os.IsNotExist(err) {
		err = os.RemoveAll(wr.filepath)
		if err != nil {
			t.Error(err)
		}
	}

	err = wr.Write(r)
	if err != nil {
		t.Error(err)
	}

	// defer func() {
	// 	if err := os.RemoveAll(TEST_SFCTPD_WRITE_PATH); err != nil {
	// 		t.Error(err)
	// 	}

	// 	if err := os.Mkdir(TEST_SFCTPD_WRITE_PATH, os.ModePerm); err != nil {
	// 		t.Error(err)
	// 	}
	// }()

	dcm_data_ref, dcm_data_pred, err := openTestDicomDatasets(REF_BATTELLE_WRITE_PATH, wr.filepath)
	if err != nil {
		t.Error(err)
	}

	// // Test important tags
	// Test Number of Detector Rows
	ref_val_int, pred_val_int, err := getIntTestElement(dcm_data_ref, dcm_data_pred, tag.NumberofDetectorRows)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(ref_val_int, pred_val_int) {
		t.Errorf("Failed because expected %v, got %v", ref_val_int, pred_val_int)
	}

	// Test Number of Detector Channels
	ref_val_int, pred_val_int, err = getIntTestElement(dcm_data_ref, dcm_data_pred, tag.NumberofDetectorColumns)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(ref_val_int, pred_val_int) {
		t.Errorf("Failed because expected %v, got %v", ref_val_int, pred_val_int)
	}

	// Test Tube Angle
	ref_val_float, pred_val_float, err := getFloatTestElement(dcm_data_ref, dcm_data_pred, tag.DetectorFocalCenterAngularPosition)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(ref_val_float, pred_val_float) {
		t.Errorf("Failed because expected %v, got %v", ref_val_float, pred_val_float)
	}

	// Test Table Position
	ref_val_float, pred_val_float, err = getFloatTestElement(dcm_data_ref, dcm_data_pred, tag.DetectorFocalCenterAxialPosition)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(ref_val_float, pred_val_float) {
		t.Errorf("Failed because expected %v, got %v", ref_val_float, pred_val_float)
	}

	// Test Data
	ref_val_pixel, pred_val_pixel, err := getPixelDataTestElement(dcm_data_ref, dcm_data_pred)
	if err != nil {
		t.Error(err)
	}

	for i, val := range ref_val_pixel {
		if (val - pred_val_pixel[0][i]) > 0.001 { // We expect at least 3 decimal precision
			t.Error("Failed because expected and predicted data do not match: ", err)
		}
	}
}

// Various convenience functions
func openTestDicomDatasets(path_v1 string, path_v2 string) (dicom.Dataset, dicom.Dataset, error) {
	// Get dicom data for reference Battelle file.
	files_v1, err := os.ReadDir(path_v1)
	if err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}
	filename_v1 := files_v1[0].Name()

	fullpath_v1 := filepath.Join(path_v1, filename_v1)
	f_v1, err := os.Open(fullpath_v1)
	if err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}

	info_v1, err := os.Stat(fullpath_v1)
	if err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}

	dcm_data_ref, err := dicom.Parse(f_v1, info_v1.Size(), nil)
	if err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}

	if err = f_v1.Close(); err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}

	// Get dicom data for test SFCTPD file.
	files_v2, err := os.ReadDir(path_v2)
	if err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}
	filename_v2 := files_v2[0].Name()

	fullpath_v2 := filepath.Join(path_v2, filename_v2)
	f_v2, err := os.Open(fullpath_v2)
	if err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}

	info_v2, err := os.Stat(fullpath_v2)
	if err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}

	dcm_data_pred, err := dicom.Parse(f_v2, info_v2.Size(), nil)
	if err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}

	if err = f_v2.Close(); err != nil {
		return dicom.Dataset{}, dicom.Dataset{}, err
	}

	return dcm_data_ref, dcm_data_pred, err
}

func getIntTestElement(dcm_data_ref dicom.Dataset, dcm_data_pred dicom.Dataset, tag_in tag.Tag) (int, int, error) {
	e_ref, err := dcm_data_ref.FindElementByTag(tag_in)
	if err != nil {
		return 0, 0, err
	}

	e_pred, err := dcm_data_pred.FindElementByTag(tag_in)
	if err != nil {
		return 0, 0, err
	}

	return int(e_ref.Value.GetValue().([]int)[0]), int(e_pred.Value.GetValue().([]int)[0]), nil
}

func getFloatTestElement(dcm_data_ref dicom.Dataset, dcm_data_pred dicom.Dataset, tag_in tag.Tag) (float64, float64, error) {
	return getNthFloatTestElement(dcm_data_ref, dcm_data_pred, tag_in, 0)
}

func getNthFloatTestElement(dcm_data_ref dicom.Dataset, dcm_data_pred dicom.Dataset, tag_in tag.Tag, N int) (float64, float64, error) {
	e_ref, err := dcm_data_ref.FindElementByTag(tag_in)
	if err != nil {
		return 0, 0, err
	}

	e_pred, err := dcm_data_pred.FindElementByTag(tag_in)
	if err != nil {
		return 0, 0, err
	}

	return float64(e_ref.Value.GetValue().([]float64)[N]), float64(e_pred.Value.GetValue().([]float64)[N]), nil
}

func getPixelDataTestElement(dcm_data_ref dicom.Dataset, dcm_data_pred dicom.Dataset) ([]float32, [][]float32, error) {
	ref_pixel_data, err := readPixelData(&dcm_data_ref, 64, 888)
	if err != nil {
		return nil, nil, err
	}

	pred_pixel_data, err := readAllPixelDataFrames(&dcm_data_pred, 64, 888)
	if err != nil {
		return nil, nil, err
	}

	return ref_pixel_data, pred_pixel_data, nil
}

func readAllPixelDataFrames(dcm_data *dicom.Dataset, detector_rows int, detector_channels int) (final_projs [][]float32, err error) {
	// More general version of readPixelDataFrames that reads all NativeFrames
	// from the SFCTPD file.

	// Extract rescale values for pixel data
	rescale_intercept, err := readDecimalString(dcm_data, tag.RescaleIntercept)
	if err != nil {
		return nil, err
	}

	rescale_slope, err := readDecimalString(dcm_data, tag.RescaleSlope)
	if err != nil {
		return nil, err
	}

	// Extract pixel data
	e, err := dcm_data.FindElementByTag(tag.PixelData)
	if err != nil {
		return nil, err
	}
	image_info := e.Value.GetValue().(dicom.PixelDataInfo)

	projs := make([][]float32, len(image_info.Frames))

	// Loop through frames and fill in projs with pixel data from each NativeFrame
	for frame_idx, curr_frame := range image_info.Frames {
		nf, err := curr_frame.GetNativeFrame()
		if err != nil {
			return nil, err
		}

		final_proj := make([]float32, nf.Rows*nf.Cols)

		if len(nf.Data[0]) > 1 {
			return nil, errors.New("multiple channels found in projection data (should be grayscale)")
		}

		// Determine if pixel data was incorrectly stored transposed
		if nf.Rows == detector_channels && nf.Cols == detector_rows {
			for i := 0; i < 64; i++ {
				for j := 0; j < detector_channels; j++ {
					dest_idx := j + i*detector_channels
					src_idx := i + j*detector_rows
					final_proj[dest_idx] = float32(rescale_intercept) + float32(rescale_slope)*float32(nf.Data[src_idx][0])
				}
			}
		} else {
			for i := range final_proj {
				final_proj[i] = float32(rescale_intercept) + float32(rescale_slope)*float32(nf.Data[i][0])
			}
		}

		projs[frame_idx] = final_proj
	}

	return projs, nil
}
