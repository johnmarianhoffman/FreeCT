package raw

type RawReader interface {
	Read(geom_overrides *ScanGeometry) ([]*Projection, *ScanGeometry, *ScanMetadata, error)
	String() string
	ProjectionReader
	ScanGeometryReader
	ScanMetadataReader
}

type rawReader struct {
	ProjectionReader
	ScanGeometryReader
	ScanMetadataReader
}

// Compose a RawReader from separate ProjectionReader and
// ScanGeometryReaders.  Generally speaking, this is probably not
// necessary for most applications.
func NewRawReader(p ProjectionReader, g ScanGeometryReader, m ScanMetadataReader) RawReader {
	return &rawReader{
		ProjectionReader:   p,
		ScanGeometryReader: g,
		ScanMetadataReader: m,
	}
}

func (r *rawReader) String() string {
	return "raw reader"
}

func (r *rawReader) Read(geom_overrides *ScanGeometry) ([]*Projection, *ScanGeometry, *ScanMetadata, error) {
	g, err := r.ScanGeometryReader.ReadGeometry()
	if err != nil {
		return nil, nil, nil, err
	}

	m, err := r.ScanMetadataReader.ReadMetadata()
	if err != nil {
		return nil, nil, nil, err
	}

	projs := make([]*Projection, 0, 2048)
	for {
		p, err := r.ProjectionReader.ReadProjection()
		if err != nil {
			return nil, nil, nil, err
		}

		if p == nil {
			break
		}

		projs = append(projs, p)
	}

	return projs, g, m, nil
}
