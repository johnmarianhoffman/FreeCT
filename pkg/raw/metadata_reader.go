package raw

type ScanMetadataReader interface {
	ReadMetadata() (*ScanMetadata, error)
}
