package raw

import (
	"bufio"
	"fmt"
	"os/exec"
	"reflect"
	"strconv"
	"strings"
	"testing"
	"time"

	"path/filepath"

	"encoding/csv"
	"os"

	"sort"

	"log"

	"github.com/matryer/is"
)

type Copier interface {
	Copy() error
	Validate() bool
	Cleanup() error

	ComputeHashes() ([]string, []string, error)
}

type CopyResult struct {
	Dataset  string          `csv:"dataset"`
	Times    []time.Duration `csv:"times"`
	Valid    []bool          `csv:"valid"`
	NumFiles int             `csv:"num_files"`
}

func (cr *CopyResult) ToStrings() []string {

	data := []string{
		cr.Dataset,
		strconv.FormatInt(int64(cr.NumFiles), 10),
	}

	for _, v := range cr.Times {
		data = append(data, strconv.FormatFloat(v.Seconds(), 'f', 2, 64))
	}

	for _, is_valid := range cr.Valid {
		if is_valid {
			data = append(data, "true")
		} else {
			data = append(data, "false")
		}
	}

	return data
}

func (cr *CopyResult) SaveTime(new_time time.Duration) {
	cr.Times = append(cr.Times, new_time)
}

func (cr *CopyResult) SaveValidity(is_valid bool) {
	cr.Valid = append(cr.Valid, is_valid)
}

func NewCopyResult(dataset_path string) *CopyResult {
	return &CopyResult{
		Dataset: dataset_path,
		Times:   make([]time.Duration, 0),
		Valid:   make([]bool, 0),
	}
}

// Assumes passwordless SSH is enabled between Source and Dest
type RsyncFileCopier struct {
	Source   string
	DestUser string
	DestHost string
	Dest     string
	NumFiles int
}

func (r *RsyncFileCopier) Copy() error {
	cmd := exec.Command("rsync", "-rv", r.Source, fmt.Sprintf("%s@%s:%s", r.DestUser, r.DestHost, r.Dest))

	cmd.Stdout = os.Stdout
	fmt.Println("copy command: ", cmd.String())
	return cmd.Run()
}

// Better hashing function
//
// This is still pretty ugly, but automatically computes both the
// local and remote hashes and automatically handles files vs
// directories
func (r *RsyncFileCopier) ComputeHashes() (local_hashes, remote_hashes []string, err error) {
	// Check if the local is a directory or a file
	info, err := os.Stat(r.Source)
	if err != nil {
		return nil, nil, err
	}

	fmt.Println("current copier:", r)

	// Configure the command to use to grab the hashes
	// The command is the only thing that needs to change between local/remote and file/dir
	var cmd *exec.Cmd
	var cmd_remote *exec.Cmd
	if info.IsDir() {
		cmd = exec.Command("bash", "-c", strings.Join([]string{"openssl", "md5", r.Source + "/*"}, " "))
		cmd_remote = exec.Command("ssh", r.DestUser+"@"+r.DestHost, fmt.Sprintf("md5sum %s/*", r.Dest))
	} else {
		cmd = exec.Command("openssl", "md5", r.Source)
		cmd_remote = exec.Command("ssh", r.DestUser+"@"+r.DestHost, fmt.Sprintf("bash -c md5sum %s", r.Dest))
	}

	fmt.Println("local hash command: ", cmd.String())
	fmt.Println("remote hash command: ", cmd_remote.String())

	// Run the local hashes
	fmt.Println("running local hashes")
	b_out := &strings.Builder{}
	cmd.Stdout = b_out

	err = cmd.Run()
	if err != nil {
		return nil, nil, err
	}

	result := b_out.String()

	result = strings.TrimSpace(result)

	scanner := bufio.NewScanner(strings.NewReader(result))

	local_hashes = make([]string, 0)
	for scanner.Scan() {
		curr_result := scanner.Text()
		parsed_result := strings.Split(curr_result, " ")

		hash := parsed_result[1]
		local_hashes = append(local_hashes, hash)
	}

	sort.Strings(local_hashes)

	// Run the remote hashes
	fmt.Println("running remote hashes")
	b_out.Reset()
	cmd_remote.Stdout = b_out

	err = cmd_remote.Run()
	if err != nil {
		return nil, nil, err
	}

	result = b_out.String()
	result = strings.TrimSpace(result)

	scanner = bufio.NewScanner(strings.NewReader(result))

	remote_hashes = make([]string, 0)
	for scanner.Scan() {
		curr_result := scanner.Text()
		parsed_result := strings.Split(curr_result, " ")

		hash := parsed_result[0]
		remote_hashes = append(remote_hashes, hash)
	}

	sort.Strings(remote_hashes)

	fmt.Println("HASH RESULTS: ", local_hashes, remote_hashes, err)

	r.NumFiles = len(local_hashes)

	return local_hashes, remote_hashes, nil
}

func (r *RsyncFileCopier) Validate() bool {

	h_src, h_dest, err := r.ComputeHashes()
	if err != nil || h_src == nil || h_dest == nil {
		log.Fatal("hash calculation failed", err)
		return false
	}

	// Compare the two sets of sorted hashes
	if !reflect.DeepEqual(h_src, h_dest) {
		return false
	}

	return true
}

func (r *RsyncFileCopier) Cleanup() error {
	cmd := exec.Command("ssh", r.DestUser+"@"+r.DestHost, fmt.Sprintf("rm -rf %s", r.Dest))
	//fmt.Println("cleanup command: ", cmd.String())
	return cmd.Run()
}

// The error handling here is kinda ugly. I don't like it, but SPIE is
// coming up and we need to get some results.  TODO: revisit at some
// point and make it better.
func DoOneCopy(c Copier, test_file, dest_file string) (time.Duration, error) {

	s := time.Now()
	err := c.Copy()
	elapsed := time.Since(s)
	if err != nil {
		return time.Duration(0), err
	}

	if valid := c.Validate(); !valid {
		return time.Duration(0), fmt.Errorf("validation failed")
	}

	err = c.Cleanup()
	if err != nil {
		return time.Duration(0), err
	}

	return elapsed, nil
}

func TestRsync(t *testing.T) {

	is := is.New(t)

	test_file := "/Users/jhoffman/Desktop/gopher.jpg"
	dest_file := "/home/john/gopher.jpg"

	c := &RsyncFileCopier{
		Source:   test_file,
		Dest:     dest_file,
		DestUser: "john",
		DestHost: "malokingi",
	}

	copy_time, err := DoOneCopy(c, test_file, dest_file)
	is.NoErr(err) // test fully copy (copy, validate, cleanup) with rsync

	t.Log("copy took:", copy_time)
}

func TestBenchmarkRsync(t *testing.T) {
	remote_host := "malokingi"
	remote_user := "john"

	runs_per_case := 5

	//files := []string{
	//	"/Users/jhoffman/Desktop/gopher.jpg",
	//	"/Users/jhoffman/Desktop/tmp",
	//}

	dest_dir := "/home/john/benchmark"

	results := make([]*CopyResult, 0)

	f_src, err := os.Open("copy-file-list.txt")
	if err != nil {
		t.Fatal("unable to load file list: ", err)
	}

	scanner := bufio.NewScanner(f_src)

	for scanner.Scan() {

		curr_file := scanner.Text()

		// Remove any trailing slashes
		// If it's a directory, we need to enforce the trailing slash
		info, err := os.Stat(curr_file)
		if err != nil {
			t.Error("stat failed:", err)
		}

		if info.IsDir() {
			curr_file = strings.TrimRight(curr_file, "/") + "/"
		}

		dest_file := filepath.Join(dest_dir, filepath.Base(curr_file))

		c := &RsyncFileCopier{
			Source:   curr_file,
			Dest:     dest_file,
			DestUser: remote_user,
			DestHost: remote_host,
		}

		copy_result := NewCopyResult(curr_file)

		for i := 0; i < runs_per_case; i++ {
			copy_time, err := DoOneCopy(c, curr_file, dest_file)
			if err != nil {
				t.Logf("copy failed (%s): %v", curr_file, err)

				copy_result.SaveTime(time.Duration(0))
				copy_result.SaveValidity(false)
				continue
			}

			copy_result.SaveTime(copy_time)
			copy_result.SaveValidity(true)
			copy_result.NumFiles = c.NumFiles

			t.Logf("copy completed (%s): %v", curr_file, copy_time)
		}

		results = append(results, copy_result)
	}

	f, _ := os.Create("results.csv")

	string_results := make([][]string, 0)

	for _, v := range results {
		string_results = append(string_results, v.ToStrings())
	}

	err = csv.NewWriter(f).WriteAll(string_results)
	if err != nil {
		t.Log("failed to save to file", err)
	}

}
