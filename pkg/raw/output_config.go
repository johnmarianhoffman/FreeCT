package raw

import (
	"fmt"
)

// Configure where the reconstruction should source its raw data
// Only ONE of these fields can be filled out for a reconstruction
type RawOutputConfig struct {
	Battelle string `json:"Batelle" yaml:"Battelle"`
	SFCTPD   string `json:"SFCTPD" yaml:"SFCTPD"`
	Binary   string `json:"Binary" yaml:"Binary"`
	//Ptr      string `json:"Ptr" yaml:"Ptr"`
	//Ctd      string `json:"Ctd" yaml:"Ctd"`
}

func (ro *RawOutputConfig) GetWriter() (RawWriter, error) {
	if ro.SFCTPD != "" {
		return NewSFCTPDWriter(ro.SFCTPD), nil
	}

	return nil, fmt.Errorf("no writer set")
}
